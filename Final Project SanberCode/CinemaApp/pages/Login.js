import React, { Component } from 'react';
import { 
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';

export default class Login extends Component {
  state = {
    username : "ryan",
    password : "tama",
    inputUsername: null,
    inputPassword: null
  }

  render() {
    return (
        <View style={styles.container}>
            <View style={{flexDirection: "column", justifyContent: "center", flex:1}}>
            <View style={styles.logo}>
                <Image source={require('../assets/img/ticket.png')} 
                style={{ width: 300, height: 150 }} />
            </View>

            <View style={styles.form}>
                <View style={styles.subform}>

                <View style={styles.userEmail}>
                    <View style={{flexDirection:"column"}}>
                    <Text style={{paddingBottom: 5, fontWeight:"bold", color:"#259299"}}>Username</Text>
                    <TextInput onChangeText={(text)=> this.setState({inputUsername: text})} style={{ height: 40, width: 250, borderRadius: 8, backgroundColor: "#259299", borderColor: '#259299', borderWidth: 2 }}></TextInput>
                    </View>
                </View>
                <View style={styles.userPassword}>
                    <View style={{flexDirection:"column"}}>
                    <Text style={{paddingBottom: 5, fontWeight:"bold", color:"#259299"}}>Password</Text>
                    <TextInput onChangeText={(text)=> this.setState({inputPassword: text})} style={{ height: 40, width: 250, borderRadius: 8, backgroundColor: "#259299", borderColor: '#259299', borderWidth: 2 }}></TextInput>
                    </View>
                </View>

                {/* <View style={styles.viewButton}>
                    <TouchableOpacity style={styles.loginButton} onPress={()=>this.props.navigation.navigate('Home')}>
                    <Text style={{textAlign: "center", color: "#0C6B71", fontWeight: "bold"}}>Login</Text>
                    </TouchableOpacity>
                </View> */}

                <View style={styles.viewButton}>
                    <TouchableOpacity style={styles.loginButton} onPress={()=>{
                      if(this.state.inputUsername == null || this.state.inputPassword == null){
                        alert('Please fill in username and password')
                      }
                      else if(this.state.inputUsername == this.state.username && this.state.inputPassword == this.state.password){
                        this.props.navigation.navigate('Home')
                      }else{
                        alert('Username and password Not Valid')
                      }
                    }}>
                    <Text style={{textAlign: "center", color: "#0C6B71", fontWeight: "bold"}}>Login</Text>
                    </TouchableOpacity>
                </View>

                </View>
            </View>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    sanberAppText: {
      marginTop: 50,
      flexDirection: "row",
      justifyContent: "center"
    },
    registryLink: {
      flexDirection: "row",
      justifyContent: "center",
      marginTop: 15
    },
    loginButton: {
      backgroundColor: '#259299',
      height: 40,
      width: 250,
      flexDirection: "column",
      justifyContent: "center",
      borderRadius: 8
    },
    viewButton:{
      flexDirection: "row",
      justifyContent: "center"
    },
    userPassword: {
      paddingTop: 10,
      paddingBottom: 15,
      flexDirection: "row",
      justifyContent: "center"
    },
    userEmail: {
      padding: 0,
      flexDirection: "row",
      justifyContent: "center"
    },
    subform: {
      flexDirection: "column",
      padding: 5,
    },
    form: {
      flexDirection: "row",
      justifyContent: "center",
      marginTop: 40
    },
    logo: {
      flexDirection: "row",
      justifyContent: "center"
    },
    container: {
      flex: 1,
      backgroundColor: "#0C6B71"
    },
  })