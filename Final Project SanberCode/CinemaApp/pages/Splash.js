import React, { Component } from 'react';
import { View, 
    Text, 
    StyleSheet, 
    Image 
} from 'react-native';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.Container}>
        <View style={styles.SubContainer}>
          <Image source={require('../assets/img/ticket.png')} 
          style={{width: 300, height: 150}}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    SubContainer: {
      flexDirection: "row",
      justifyContent: "center"
    },
    Container: {
      flex: 1,
      backgroundColor: "#0C6B71",
      justifyContent: "center"
    }
  })