import React, { Component } from 'react';
import { View, Text } from 'react-native';

import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from './Login'
import Home from './Home'
import About from './About'
import Details from './Details'

const stackNavigator = createStackNavigator()
const TabNavigator = createBottomTabNavigator()
const StackHome = createStackNavigator()

const StckHome = () => (
    <StackHome.Navigator screenOptions={{headerStyle:{backgroundColor:"#0C6B71"}, headerTitleStyle:{color:'white'},}}>
      <StackHome.Screen options={{headerShown: false}} name="Home" component={Home}/>
      <StackHome.Screen name="Details" component={Details}/>
    </StackHome.Navigator>
)

const Tab = () => (
  <TabNavigator.Navigator tabBarOptions={{activeTintColor: 'white', 
    inactiveTintColor: '#259299', 
    labelStyle:{fontSize: 12, fontWeight: "bold"},
    style:{backgroundColor:'#0C6B71'}
  }}>
    <TabNavigator.Screen options={{tabBarIcon: ({color, size})=>(<Icon name="home" color={color} size={size}/>)}} name="Home" component={StckHome} />
    <TabNavigator.Screen options={{tabBarIcon: ({color, size})=>(<Icon name="account-circle" color={color} size={size}/>)}} name="About" component={About} />
  </TabNavigator.Navigator>
)

export default class Index extends Component {
  render() {
    return (
      <NavigationContainer>
        <stackNavigator.Navigator screenOptions={{headerShown: false}} initialRouteName="Login">
          <stackNavigator.Screen name="Login" component={Login}/>
          <stackNavigator.Screen name="Home" component={Tab}/>
        </stackNavigator.Navigator>
      </NavigationContainer>
    );
  }
}
