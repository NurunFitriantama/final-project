import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Image,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class Home extends Component {
  state = {
      data: []
  }

  componentWillMount(){
    this.fetchData()
  }

  fetchData = async()=> {
      const response = await fetch('https://www.breakingbadapi.com/api/episodes/')
      const json = await response.json()
      this.setState({data: json})
  }

  render() {
    return (
      <View style={styles.Container}>
        <View style={styles.Header}>
          <View style={styles.SubHeader}>

          </View>
          <Text style={{
            fontSize: 30,
            color: 'white'}}>
              Breaking Bad Movie
          </Text>
        </View>
        
        
        <ScrollView>
            <FlatList
            data={this.state.data}
            keyExtractor={(x, i)=> i}
            renderItem={({ item }) =>
            <View style={styles.Main}>
                <TouchableOpacity style={styles.ViewItems} onPress={()=>this.props.navigation.navigate('Details', {key:item.episode_id})}>
                    <View style={styles.SubViewItems}>
                    <Image 
                        source={{uri : "https://m.media-amazon.com/images/M/MV5BMjhiMzgxZTctNDc1Ni00OTIxLTlhMTYtZTA3ZWFkODRkNmE2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg"}}
                        style={{height: 100, width: 70, borderRadius: 8}}
                    />
                    <View style={styles.DescItems}>
                        <Text style={{fontSize: 20, 
                            color: "#0C6B71", 
                            fontWeight: "bold",
                            flexWrap:"wrap"}}>
                            {item.title.length < 3000 ? `${item.title}` : `${item.title.substring(0, 27)}...`}
                        </Text>
                        <Text style={{fontSize: 16, 
                            color: "#0C6B71"}}>
                            Episode {item.episode} ~ Season {item.season}
                        </Text>
                    </View>
                    </View>
                </TouchableOpacity>
            </View>
            }
            
            />
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
    DescItems: {
        flexDirection: "column",
        justifyContent: "center",
        paddingHorizontal: 20
    },
    SubViewItems: {
        flexDirection: "row"
        
    },
    ViewItems: {
        marginTop: 10,
        backgroundColor: "#9FD7DB",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 8
    },
    Main: {
        flexDirection: "column",
        paddingHorizontal: 20,
    },
    SubHeader:{
        flexDirection: "row"
    },
    Header: {
        backgroundColor: '#0C6B71',
        height: 60,
        paddingHorizontal: 20,
        flexDirection: "column",
        justifyContent: "center"
    },
    Container: {
        flex: 1,
        backgroundColor: "#CBE8EA"
    }
})