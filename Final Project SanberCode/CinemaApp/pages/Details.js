import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    FlatList
} from 'react-native';
import { TextInput } from 'react-native-paper';

export default class Details extends Component {
    state = {
        episodes_id: this.props.route.params.key,
        data: []
    }
    componentWillMount(){
        this.fetchData()
    }
    fetchData = async()=>{
        const response = await fetch(`https://www.breakingbadapi.com/api/episodes/${this.state.episodes_id}`)
        const json = await response.json()
        this.setState({data: json})
    }
  render() {
    return (

        <FlatList style={{flex: 1, backgroundColor: "#CBE8EA"}}
          data = {this.state.data}
          keyExtractor = {(x, i)=> i}
          renderItem={({ item }) => 
          <View style={styles.Container}>
            <View style={styles.ViewTitle}>
                <Text style={{fontWeight: "bold", fontSize: 20}}>Title :</Text>
                <View style={styles.DescTitle}>
                    <Text style={{fontSize: 15}}>{item.title}</Text>
                </View>
            </View>
    
            <View>
                <View style={styles.ViewTitle}>
                <Text style={{fontWeight: "bold", fontSize: 20}}>Air Date :</Text>
                    <View style={styles.DescTitle}>
                        <Text style={{fontSize: 15}}>{item.air_date}</Text>
                    </View>
                </View>
            </View>
    
            <View>
                <View style={styles.ViewTitle}>
                <Text style={{fontWeight: "bold", fontSize: 20}}>Characters :</Text>
                    <View style={styles.DescTitle}>
                        <Text style={{fontSize: 15}}>{item.characters}</Text>
                    </View>
                </View>
            </View>
    
            <View>
                <View style={styles.ViewTitle}>
                <Text style={{fontWeight: "bold", fontSize: 20}}>Series :</Text>
                    <View style={styles.DescTitle}>
                        <Text style={{fontSize: 15}}>{item.series}</Text>
                    </View>
                </View>
            </View>
    
            </View>
        }
        />
      
    );
  }
}

const styles = StyleSheet.create({
    DescTitle: {
      backgroundColor: "#9FD7DB",
      padding: 20,
      borderRadius: 8
    },
    ViewTitle: {
        marginTop: 10
    },
    Container: {
      flex: 1,
      backgroundColor: "#CBE8EA",
      padding: 10,
    }
})