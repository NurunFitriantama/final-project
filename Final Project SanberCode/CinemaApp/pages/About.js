import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet,
  Image
} from 'react-native';

export default class About extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <View style={styles.header}>
            <Text style={{fontSize: 35, fontWeight: "bold", textAlign: "center"}}>About Me</Text>
          </View>

          <View style={styles.Main}>
            <View style={styles.ProfilePict}>
              <Image source={require('../assets/img/IMG_0404.jpg')} style={{height: 150, width: 150, borderRadius: 75, position: "relative"}}/>
            </View>
            <View style={styles.Name}>
              <Text style={{fontSize: 20, fontFamily: "", fontWeight: "bold", textAlign: "center"}}>Moh. Nurun Fitriantama</Text>
            </View>
            <View style={styles.Medsos}>
              <View style={styles.subMedsos}>
                <Image style={styles.MedsosImg} source={require('../assets/img/fb.png')}/>
                <Text style={styles.MedsosText}>https://web.facebook.com/ryan.dosisjunior/</Text>
              </View>
              <View style={styles.subMedsos}>
                <Image style={styles.MedsosImg} source={require('../assets/img/tw.png')}/>
                <Text style={styles.MedsosText}>https://twitter.com/MohNurunF</Text>
              </View>
              <View style={styles.subMedsos}>
                <Image style={styles.MedsosImg} source={require('../assets/img/ig.png')}/>
                <Text style={styles.MedsosText}>https://www.instagram.com/mohnurunf/</Text>
              </View>
              <View style={styles.subMedsos}>
                <Image style={styles.MedsosImg} source={require('../assets/img/gitlab.png')}/>
                <Text style={styles.MedsosText}>https://gitlab.com/NurunFitriantama</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Name:{
    textAlign: "center"
  },
  MedsosText:{
    fontFamily: "",
    fontSize: 16,
    fontWeight: "bold",
    marginTop: 15,
    marginLeft: 20
  },
  MedsosImg: {
    height: 50,
    width: 50
  },
  subMedsos: {
    flexDirection: "row",
    paddingTop: 20
  },
  Medsos: {
    paddingLeft: 20
  },
  ProfilePict: {
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: 20,
    marginTop: -50
  },
  Main:{
    flexDirection: "column",
    backgroundColor: "#26A9E1",
    flex: 1,
  },
  header:{
    textAlign: "center",
    paddingTop: 20,
    paddingBottom: 60
  },
  subContainer:{
    flexDirection: "column",
    justifyContent: "center",
    flex: 1
  },
  container:{
    flex: 1
  }
})