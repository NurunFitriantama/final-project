import React, { Component } from 'react';
import { View, 
  Text,
  StyleSheet
} from 'react-native';

import Splash from './pages/Splash';
import Index from './pages/Index';
import Home from './pages/Home'
import Details from './pages/Details'
import Login from './pages/Login'

export default class App extends Component {
  
  componentWillMount(){
    this.state = {
      view : <Splash />
    }
  
    setTimeout(() => {
      if(true){
        this.setState({
          view : <Index />
        })
      }
    }, 3000)
  }

  render() {
    return (
      this.state.view
      // <Index />
      // <Home />
      // <Details />
      // <Login />
    );
  }
}

